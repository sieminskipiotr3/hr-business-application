This is a sample application for managing company's human resources.

Built with implementation of functional programming, focused on lambda functions usage.

It provides sorting methods, hierarchy, custom access rights (for granting bonuses for example)
and is built in a scalable way. 
