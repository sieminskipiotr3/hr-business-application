package psieminski.interfaces;

import psieminski.Employee;
import psieminski.Manager;
import psieminski.Subordinate;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public interface PeopleManageRights {

    List<Employee> getAllEmployees();

    //returns list of subordinates of a given manager
    default List<Subordinate> subordinatesOfManager(Manager manager) {
        if (manager.getSubordinates_list() == null) {manager.setSubordinates_list(new ArrayList<>());}
        return manager.subordinates_list;
    }
    //returns list of employees which names begin with specified letter
    default List<Subordinate> employeesSurnameBeginsWithX(String begins_with, Manager manager) {

        List<Subordinate> subordinates = subordinatesOfManager(manager);

        return subordinates
                .stream()
                .filter(s -> s.getLast_name().startsWith(begins_with))
                .collect(Collectors.toList());
    }

    //counts cost of all bonuses of all employees within the company
    default BigDecimal countCostAllBonus() {

        List<Employee> employees = getAllEmployees();
        return employees
                .stream()
                .map(Employee::getAnnualBonus)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

    }

    //returns list of employees who earn more than specified amount
    default List<Employee> earnMoreThanN(BigDecimal moreThan) {

        List<Employee> employees = getAllEmployees();

        return employees
                .stream()
                .filter(e -> e.getSalary().compareTo(moreThan) == 1)
                .collect(Collectors.toList());
    }

    default BigDecimal highestSalaryNoBonus() {

        List<Employee> employees = getAllEmployees();

        return employees
                .stream()
                .map(Employee::getSalary)
                .max(Comparator.naturalOrder()).orElseThrow(NoSuchElementException::new);

    }

    default BigDecimal highestSalaryWithBonus() {

        List<Employee> employees = getAllEmployees();

        return employees
                .stream()
                .map(e -> e.getSalary().add(e.getAnnualBonus()))
                .max(Comparator.naturalOrder()).orElseThrow(NoSuchElementException::new);
    }

    default Employee findLongestSeniority() {

        List<Employee> employees = getAllEmployees();

        return employees.stream().max(Comparator.comparing(Employee::calculateSeniority)).get();
    }


    default void assignBirthDate(Employee e, String bDate) {
        e.setBirth_date(bDate);
    }

    default void assignTerminationDate(Employee e, String tDate) {
        e.setTermination_date(tDate);
    }

    default void assignHireDate(Employee e, String hDate) {
        e.setHire_date(hDate);
    }

}
