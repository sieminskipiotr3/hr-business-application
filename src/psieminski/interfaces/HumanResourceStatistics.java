package psieminski.interfaces;

import psieminski.Employee;
import psieminski.Intern;
import psieminski.enums.bonus_plan;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public interface HumanResourceStatistics { //I'm implementing this as an interface as I want to give it to specific people only

    default List<Employee> olderThanAndEarnMore(List<Employee> allEmployees, Employee employee) {

        return allEmployees
                .stream()
                .filter(x -> x.getSalary().compareTo(employee.getSalary()) > 0)
                .filter(x -> x.calculateAge() > employee.calculateAge())
                .collect(Collectors.toList());
    }
    default List<Intern> practiceLengthLongerThan(List<Intern> allInterns, long daysCount) {

            allInterns
                .stream()
                .filter(x -> x.calculateSeniority() > daysCount)
                .forEach(x -> x.setSalary(x.getSalary().multiply(BigDecimal.valueOf(1.05))));

        return allInterns
                .stream()
                .filter(x -> x.calculateSeniority() > daysCount)
                .collect(Collectors.toList());

    }
    default List<Employee> seniorityLongerThan(List<Employee> allEmployees, long monthCount) {

        allEmployees
                .stream()
                .filter(x -> x.calculateSeniorityInMonths() > monthCount)
                .filter(x -> x.getAnnualBonus().compareTo(BigDecimal.valueOf(300)) < 0)
                .forEach(x -> x.setBonus(bonus_plan.annual_manager)); //I'm setting to manager's plan as I have bonus plans implemented instead of just values

        return allEmployees
                .stream()
                .filter(x -> x.calculateSeniorityInMonths() > monthCount)
                .collect(Collectors.toList());
    }
    default List<Employee> seniorityBetweenOneAndThreeYears(List<Employee> allEmployees) {

            allEmployees
                .stream()
                .filter(x -> 1 < (int) x.calculateSeniorityInYears() && (int) x.calculateSeniorityInYears() < 3)
                .forEach(x -> x.setSalary(x.getSalary().multiply(BigDecimal.valueOf(1.1))));

            return allEmployees
                .stream()
                .filter(x -> 1 < (int) x.calculateSeniorityInYears() && (int) x.calculateSeniorityInYears() < 3)
                    .collect(Collectors.toList());
    }
    default List<Employee> seniorityLongerThan(List<Employee> allEmployees, Employee employee) {

            allEmployees
                .stream()
                .filter(x -> x.calculateSeniority() > employee.calculateSeniority())
                .filter(x -> x.getSalary().compareTo(employee.getSalary()) > 0)
                .forEach(x -> x.setSalary(employee.getSalary()));

            return allEmployees
                .stream()
                .filter(x -> x.calculateSeniority() > employee.calculateSeniority())
                .filter(x -> x.getSalary().compareTo(employee.getSalary()) < 0)
                    .collect(Collectors.toList());
    }
    default List<Employee> seniorityBetweenTwoAndFourYearsAndAgeGreaterThan(List<Employee> allEmployees, short age) {
        return allEmployees
                .stream()
                .filter(x -> 2 < (int) x.calculateSeniority() && x.calculateSeniority() < 4)
                .filter(x -> x.getAge() > age)
                .collect(Collectors.toList());
    }

}
