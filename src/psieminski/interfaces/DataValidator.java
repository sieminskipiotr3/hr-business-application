package psieminski.interfaces;

import java.time.LocalDate;

public interface DataValidator {

    //ensures correct date format is inserted
    default Boolean validateDateInput(String dateString) {
        try {
            LocalDate.parse(dateString);
        } catch (Exception exception) {
            return false;
        }
        return true;
    }
}
