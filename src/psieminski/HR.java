package psieminski;

import psieminski.enums.Department;
import psieminski.enums.bonus_plan;
import psieminski.interfaces.HumanResourceStatistics;
import psieminski.interfaces.PeopleManageRights;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HR extends Specialist implements PeopleManageRights, HumanResourceStatistics {


    public HR(String first_name, String last_name, LocalDate birth_date,
              bonus_plan bonus, Department department, BigDecimal salary, BigDecimal tax_rate,
              Company company, Integer id, Manager manager) {
        super(first_name, last_name, birth_date ,bonus, department, salary, tax_rate, company, id, manager);
    }

    @Override
    public List<Employee> getAllEmployees() {
        if (this.company.employees == null) {this.company.setEmployees(new ArrayList<>());}
        return this.company.employees;
    }

    public Payroll preparePayroll(Employee e, Integer id, Boolean applyAnnualBonus,
                                  Boolean applySpotBonus, BigDecimal spotBonusAmount, BigDecimal overtimeHours) {

        Payroll payroll = new Payroll(e,id,applyAnnualBonus,applySpotBonus, spotBonusAmount, overtimeHours);

        if(applyAnnualBonus) {payroll.addAnnualBonus();}
        payroll.calculateGrossPayment();
        payroll.calculateNetPayment();

        return payroll;

    }

    public Map<Employee, Payroll> calculatePayrollManyEmployees (List<Employee> employees,
                                                                 List<Integer> ids, List<Boolean> applyAnnualBonus,
                                                                 List<Boolean> applySpotBonus, List<BigDecimal> spotBonusAmount,
                                                                 List<BigDecimal> overtimeHours) {

        Map<Employee, Payroll> payroll_list = new HashMap<Employee, Payroll>();

        for (int i = 0; i < employees.size(); i++) {
            Employee e = employees.get(i);
            Integer id = ids.get(i);
            Boolean annualBonus = applyAnnualBonus.get(i);
            Boolean spotBonus = applySpotBonus.get(i);
            BigDecimal spotBonusSum = spotBonusAmount.get(i);
            BigDecimal overtime = overtimeHours.get(i);
            Payroll payroll = preparePayroll(e, id, annualBonus, spotBonus, spotBonusSum, overtime);
            payroll_list.put(e, payroll);
        }

        return payroll_list;

    }

    public void assignEmployeesToManager(Manager manager, List<Subordinate> employees) {
        manager.setSubordinates_list(employees);
        for (Subordinate s : employees) {
            s.setManager(manager);
        }
    }

    public void assignOneToOneManagerSubordinate(Manager manager, Subordinate s) {
        manager.getSubordinates_list().add(s);
        s.setManager(manager);
    }

    public void setHireDateForEmployee(Employee e, String hireDate) {
        e.setHire_date(hireDate);
    }

}
