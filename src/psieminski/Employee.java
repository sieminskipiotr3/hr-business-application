package psieminski;

import psieminski.enums.Department;
import psieminski.enums.bonus_plan;
import psieminski.interfaces.DataValidator;

import java.math.BigDecimal;
import java.math.MathContext;
import java.time.LocalDate;

import static java.time.temporal.ChronoUnit.*;

public abstract class Employee implements DataValidator {

    private String first_name;
    private String last_name;
    private LocalDate birth_date;
    private LocalDate hire_date;
    private LocalDate termination_date;
    private bonus_plan bonus;
    private Department department;
    private BigDecimal salary;
    private BigDecimal tax_rate;
    public Company company;
    public Integer id;
    public Manager manager;
    private BigDecimal hasBonus;

    public Employee(String first_name, String last_name, LocalDate birth_date,
                    bonus_plan bonus, Department department, BigDecimal salary, BigDecimal tax_rate,
                    Company company, Integer id, Manager manager) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.birth_date = birth_date;
        this.bonus = bonus;
        this.department = department;
        this.salary = salary;
        this.tax_rate = tax_rate;
        this.company = company;
        this.id = id;
        this.manager = manager;
        this.hasBonus = salary.multiply(getAnnualBonus());
    }

    public BigDecimal getTax_rate() {
        return tax_rate;
    }

    public void setTax_rate(BigDecimal tax_rate) {
        this.tax_rate = tax_rate;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public LocalDate getBirth_date() {
        return birth_date;
    }

    public LocalDate getHire_date() {
        return hire_date;
    }

    public LocalDate getTermination_date() {
        return termination_date;
    }

    public bonus_plan getBonusType() {
        return bonus;
    }

    public BigDecimal getAnnualBonus() {
        return salary.multiply(bonus.bonus_percentage);
    }

    public Department getDepartment() {
        return department;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public short getAge() {
        LocalDate today = LocalDate.now();
        return (short) YEARS.between(birth_date, today);

    }

    public void setTermination_date(String termination_date) {
        if (validateDateInput(termination_date)) {
            this.termination_date = LocalDate.parse(termination_date);
        } else {
            throw new IllegalArgumentException("Date format should be 'yyyy-MM-dd'");
        }
    }

    public void setBirth_date(String birth_date) {
        if (validateDateInput(birth_date)) {
            this.birth_date = LocalDate.parse(birth_date);
        } else {
            throw new IllegalArgumentException("Date format should be 'yyyy-MM-dd'");
        }
    }

    public void setHire_date(String hire_date) {
        if (validateDateInput(hire_date)) {
            this.hire_date = LocalDate.parse(hire_date);
        } else {
            throw new IllegalArgumentException("Date format should be 'yyyy-MM-dd'");
        }
    }

    public void setBonus(bonus_plan bonus) {
        this.bonus = bonus;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public Company getCompany() {
        return company;
    }

    public Integer getId() {
        return id;
    }


    public long calculateSeniority() {
        if (hire_date == null) {throw new IllegalStateException("No Hire Date set");}
        LocalDate today = LocalDate.now();
        LocalDate hireDate = this.hire_date;
        return DAYS.between(hireDate, today);
    }


    public long calculateAge() {
        LocalDate today = LocalDate.now();
        LocalDate bDay = this.birth_date;
        return YEARS.between(bDay, today);
    }

    public long calculateSeniorityInYears() {
        if(hire_date == null) {throw new IllegalStateException("Please set hireDate");}
        LocalDate today = LocalDate.now();
        LocalDate hireDate = this.hire_date;
        return YEARS.between(hireDate, today);
    }

    public long calculateSeniorityInMonths() {
        if(hire_date == null) {throw new IllegalStateException("Please set hireDate");}
        LocalDate today = LocalDate.now();
        LocalDate hireDate = this.hire_date;
        return MONTHS.between(hireDate, today);
    }

    public Boolean isOlderThan(Employee e) {
        return this.getAge() > e.getAge();
    }

    public Boolean isYoungerThan(Employee e) {
        return this.getAge() < e.getAge();
    }

    public String compareAge(Employee e) {
        if (this.getAge() > e.getAge()) {
            return this.getLast_name() + " is older than " + e.getLast_name();
        } else if (this.getAge() < e.getAge()) {
            return this.getLast_name() + " is younger than " + e.getLast_name();
        } else {
            return "Their age are the same";
        }
    }

    public Boolean seniorityLongerThanYears(long yearsCount){
        return calculateSeniorityInYears() > yearsCount;
    }

    public Boolean seniorityLongerThanMonths(long monthsCount) {
        return calculateSeniorityInMonths() > monthsCount;
    }

    public Boolean bonusGreaterThan(BigDecimal threshold) {
        return hasBonus.compareTo(threshold) > 0;
    }

}
