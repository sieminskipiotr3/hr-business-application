package psieminski.enums;

import java.math.BigDecimal;

public enum bonus_plan {

    annual_subordinate(BigDecimal.valueOf(0.1)),
    annual_manager(BigDecimal.valueOf(.15)),
    annual_executive(BigDecimal.valueOf(0.2));

    public final BigDecimal bonus_percentage;

    public BigDecimal getBonus_percentage() {
        return bonus_percentage;
    }

    private bonus_plan(BigDecimal bonus_percentage) {
        this.bonus_percentage = bonus_percentage;
    }
}
