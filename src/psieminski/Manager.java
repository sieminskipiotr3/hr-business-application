package psieminski;

import psieminski.enums.Department;
import psieminski.enums.bonus_plan;
import psieminski.interfaces.PeopleManageRights;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Manager extends Employee implements PeopleManageRights {

    public List<Subordinate> subordinates_list;

    public Manager(String first_name, String last_name, LocalDate birth_date,
                   bonus_plan bonus, Department department, BigDecimal salary, BigDecimal tax_rate,
                   Company company, Integer id, Manager manager , List<Subordinate> subordinates_list) {
        super(first_name, last_name, birth_date, bonus, department, salary, tax_rate, company, id, manager);
        this.subordinates_list = subordinates_list;
    }

    public List<Subordinate> getSubordinates_list() {
        if (subordinates_list == null) {setSubordinates_list(new ArrayList<Subordinate>());}
        return subordinates_list;
    }

    public void setSubordinates_list(List<Subordinate> subordinates_list) {
        this.subordinates_list = subordinates_list;
    }

    @Override
    public List<Employee> getAllEmployees() {
        if (this.company.employees == null) {this.company.setEmployees(new ArrayList<Employee>());}
        return this.company.employees;
    }

    public List<Employee> getAllSubordinates() {
        return getAllEmployees()
                .stream()
                .filter(x -> x.getClass() != this.getClass())
                .collect(Collectors.toList());
    }

}
