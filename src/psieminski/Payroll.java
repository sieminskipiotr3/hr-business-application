package psieminski;

import psieminski.interfaces.DataValidator;

import java.math.BigDecimal;
import java.math.MathContext;
import java.time.LocalDate;

public class Payroll implements DataValidator {

    private LocalDate date;
    private Employee employee;
    private Integer id;
    private Boolean applyAnnualBonus;
    private Boolean applySpotBonus;
    private BigDecimal spotBonusAmount;
    private BigDecimal overtimeHours;
    private BigDecimal grossPayout;
    private BigDecimal netPayout;

    MathContext m = new MathContext(2);

    public Payroll(Employee employee, Integer id, Boolean applyAnnualBonus, Boolean applySpotBonus, BigDecimal spotBonusAmount, BigDecimal overtimeHours) {
        this.employee = employee;
        this.id = id;
        this.applyAnnualBonus = applyAnnualBonus;
        this.applySpotBonus = applySpotBonus;
        this.spotBonusAmount = spotBonusAmount;
        this.overtimeHours = overtimeHours;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(String dateString) {
        if (validateDateInput(dateString)) {
            this.date = LocalDate.parse(dateString);
        } else {
            throw new IllegalArgumentException("Date format should be 'yyyy-MM-dd'");
        }
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getApplyAnnualBonus() {
        return applyAnnualBonus;
    }

    public void setApplyAnnualBonus(Boolean applyAnnualBonus) {
        this.applyAnnualBonus = applyAnnualBonus;
    }

    public Boolean getApplySpotBonus() {
        return applySpotBonus;
    }

    public void setApplySpotBonus(Boolean applySpotBonus) {
        this.applySpotBonus = applySpotBonus;
    }

    public BigDecimal getSpotBonusAmount() {
        return spotBonusAmount;
    }

    public void setSpotBonusAmount(BigDecimal spotBonusAmount) {
        this.spotBonusAmount = spotBonusAmount;
    }

    public BigDecimal getGrossPayout() {
        if (grossPayout == null) {setGrossPayout(BigDecimal.ZERO);}
        return grossPayout;
    }

    private void setGrossPayout(BigDecimal grossPayout) {
        this.grossPayout = grossPayout.round(m);
    }

    public BigDecimal getNetPayout() {
        return netPayout;
    }

    private void setNetPayout(BigDecimal netPayout) {
        this.netPayout = netPayout.round(m);
    }

    public BigDecimal getOvertimeHours() {
        return overtimeHours;
    }

    public void setOvertimeHours(BigDecimal overtimeHours) {
        this.overtimeHours = overtimeHours.round(m);
    }

    public BigDecimal getHourlyPay() {
        MathContext m = new MathContext(2);
        return employee.getSalary().divide(BigDecimal.valueOf(52), m).divide(BigDecimal.valueOf(5), m).divide(BigDecimal.valueOf(8), m);
    }

    public BigDecimal getMonthlyPay() {
        return employee.getSalary().divide(BigDecimal.valueOf(12), m);
    }

    public void calculateGrossPayment() {
        BigDecimal monthly = getMonthlyPay();
        BigDecimal overtime = getOvertimeHours().multiply(getHourlyPay());
        BigDecimal spot = getSpotBonusAmount();
        BigDecimal gross = monthly.add(overtime).add(spot).round(m);
        setGrossPayout(getGrossPayout().add(gross));
    }

    public void addAnnualBonus() {
        setGrossPayout(getGrossPayout().add(employee.getAnnualBonus()));
    }

    public void calculateNetPayment() {
        setNetPayout(getGrossPayout().multiply(BigDecimal.ONE.subtract(employee.getTax_rate())));
    }

}
