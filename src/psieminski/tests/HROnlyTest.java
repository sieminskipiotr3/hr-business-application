package psieminski.tests;

import psieminski.*;
import psieminski.enums.Department;
import psieminski.enums.bonus_plan;
import psieminski.enums.intern_type;

import java.math.BigDecimal;
import java.math.MathContext;
import java.time.LocalDate;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class HROnlyTest {

    @org.junit.jupiter.api.Test
    void preparePayroll() {

        var company = new Company("Apple");

        var manager = new Manager("Andrew", "Manager", LocalDate.parse("1968-11-30"), bonus_plan.annual_manager, Department.IT, BigDecimal.valueOf(65000.00), BigDecimal.valueOf(0.49),
                company, 2, null, null);
        var hr_rep = new HR("John", "Testing", LocalDate.parse("1971-05-02") , bonus_plan.annual_subordinate, Department.HR, BigDecimal.valueOf(35000.00), BigDecimal.valueOf(0.31),
                            company, 1, manager);

        var payroll = hr_rep.preparePayroll(hr_rep, 1, true, true, BigDecimal.valueOf(1500.00), BigDecimal.valueOf(10.5));

        BigDecimal hourly = BigDecimal.valueOf(((35000.00 / 52) / 5) / 8);
        BigDecimal monthly = BigDecimal.valueOf(35000.00 / 12);

        BigDecimal gross = hr_rep.getAnnualBonus().add(monthly).add((hourly.multiply(BigDecimal.valueOf(10.5))).add(BigDecimal.valueOf(1500.00))).round(new MathContext(2));
        BigDecimal net = gross.multiply((BigDecimal.ONE.subtract(hr_rep.getTax_rate()))).round(new MathContext(2));


        assertEquals(true, payroll.getApplyAnnualBonus());
        assertEquals(true, payroll.getApplySpotBonus());
        assertEquals(BigDecimal.valueOf(1500.00), payroll.getSpotBonusAmount());
        assertEquals(BigDecimal.valueOf(10.5), payroll.getOvertimeHours());
        assertEquals(gross, payroll.getGrossPayout());
        assertEquals(net, payroll.getNetPayout());

    }

    @org.junit.jupiter.api.Test
    void payrollForManyEmployees() {

        var company = new Company("Apple");

        var manager = new Manager("Andrew", "Manager", LocalDate.parse("1968-11-30") ,bonus_plan.annual_manager, Department.IT, BigDecimal.valueOf(65000.00), BigDecimal.valueOf(0.49),
                company, 2, null, null);
        var executive = new Manager("Theo", "The Boss", LocalDate.parse("1988-10-13") ,bonus_plan.annual_executive, Department.HR, BigDecimal.valueOf(135000.00), BigDecimal.valueOf(0.64),
                company, 3, null, null);
        var hr_rep = new HR("John", "From HR", LocalDate.parse("1971-05-02") ,bonus_plan.annual_subordinate, Department.HR, BigDecimal.valueOf(35000.00), BigDecimal.valueOf(0.31),
                company, 1, executive);
        var it_guy = new IT("Carl", "From IT", LocalDate.parse("1985-05-02") ,bonus_plan.annual_subordinate, Department.IT, BigDecimal.valueOf(69000.00), BigDecimal.valueOf(0.34),
                company, 4, manager);
        var intern = new Intern("Lark", "The Intern", LocalDate.parse("2000-05-02") ,bonus_plan.annual_subordinate, Department.IT, BigDecimal.valueOf(14000), BigDecimal.valueOf(0.19),
                company, 5, manager, intern_type.six_months);

        company.employees.add(hr_rep);
        company.employees.add(manager);
        company.employees.add(executive);
        company.employees.add(it_guy);
        company.employees.add(intern);

        List<Employee> employees = hr_rep.getAllEmployees();
        List<Integer> ids = new ArrayList<Integer>();
        for (Employee e : employees) {ids.add(e.getId());}
        List<Boolean> annual = new ArrayList<Boolean>();
        for (Employee e : employees) {annual.add(false);}
        List<Boolean> spot = new ArrayList<Boolean>();
        for (Employee e : employees) {spot.add(true);}
        List<BigDecimal> spotAmount = new ArrayList<BigDecimal>();
        for (int i = 0; i < employees.size(); i++) {spotAmount.add(BigDecimal.valueOf(i).add(BigDecimal.valueOf(500.00)));}
        List<BigDecimal> overtime = new ArrayList<BigDecimal>();
        for (int j = 0; j < employees.size(); j++) {overtime.add(BigDecimal.valueOf(j).add(BigDecimal.valueOf(10.00)));}
        Map<Employee, Payroll> map = hr_rep.calculatePayrollManyEmployees(employees,
                ids, annual, spot, spotAmount, overtime);

        BigDecimal exec_spot = BigDecimal.valueOf(502.00);
        BigDecimal exec_overtime = BigDecimal.valueOf(12.00);
        Payroll p_executive = hr_rep.preparePayroll(executive, 2, false, true, exec_spot,
                exec_overtime);

        assertEquals(p_executive.getNetPayout(), map.get(executive).getNetPayout());

    }

    @org.junit.jupiter.api.Test
    void assignOneToOne() {

        var company = new Company("Apple");

        var manager = new Manager("Andrew", "Manager", LocalDate.parse("1968-11-30") , bonus_plan.annual_manager, Department.IT, BigDecimal.valueOf(65000.00), BigDecimal.valueOf(0.49),
                company, 2, null, null);
        var hr_rep = new HR("John", "Testing", LocalDate.parse("1971-05-02") , bonus_plan.annual_subordinate, Department.HR, BigDecimal.valueOf(35000.00), BigDecimal.valueOf(0.31),
                company, 1, null);

        hr_rep.assignOneToOneManagerSubordinate(manager, hr_rep);

        assertEquals(manager, hr_rep.getManager());
        assertEquals(hr_rep, manager.getSubordinates_list().get(0));

    }
}