package psieminski.tests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import psieminski.*;
import psieminski.enums.Department;
import psieminski.enums.bonus_plan;
import psieminski.enums.intern_type;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class HumanResourceStatisticsTest { //tests related to days may fail if run at a later date because of using now() method for calculations

    private Company company = new Company("Apple");
    private Manager manager = new Manager("Andrew", "A Manager", LocalDate.parse("1968-11-30") , bonus_plan.annual_manager, Department.IT, BigDecimal.valueOf(65000.00), BigDecimal.valueOf(0.49),
            company, 2, null, null);
    private Manager executive = new Manager("Theo", "The Boss", LocalDate.parse("1948-11-30") , bonus_plan.annual_executive, Department.HR, BigDecimal.valueOf(135000.00), BigDecimal.valueOf(0.64),
            company, 3, null, null);
    private HR hr_rep = new HR("John", "From HR", LocalDate.parse("1971-05-02") ,bonus_plan.annual_subordinate, Department.HR, BigDecimal.valueOf(35000.00), BigDecimal.valueOf(0.31),
            company, 1, executive);
    private IT it_guy = new IT("Carl", "From IT", LocalDate.parse("1985-05-02") ,bonus_plan.annual_subordinate, Department.IT, BigDecimal.valueOf(69000.00), BigDecimal.valueOf(0.34),
            company, 4, manager);
    private Intern intern = new Intern("Lark", "An Intern", LocalDate.parse("2000-05-02") ,bonus_plan.annual_subordinate, Department.IT, BigDecimal.valueOf(14000.00), BigDecimal.valueOf(0.19),
            company, 5, manager, intern_type.six_months);

    List<Employee> allEmployees = hr_rep.getAllEmployees();

    private void assignEmployeesToCompany(){
        company.employees.add(manager);
        company.employees.add(executive);
        company.employees.add(hr_rep);
        company.employees.add(it_guy);
        company.employees.add(intern);
    }

    private void assignHireDate() {
        manager.assignHireDate(hr_rep, "2021-01-01");
        manager.assignHireDate(it_guy, "2021-02-02");
        manager.assignHireDate(intern, "2021-03-03");
        manager.assignHireDate(manager, "2020-12-12");
        manager.assignHireDate(executive, "2020-11-11");
    }

    @BeforeEach
    void assign(){
        assignEmployeesToCompany();
        assignHireDate();
    }

    @Test
    void olderThanAndEarnMore() {
        var hr = hr_rep;
        var olderThanAndEarnMoreList = new ArrayList<Employee>();
        olderThanAndEarnMoreList.add(manager);
        olderThanAndEarnMoreList.add(executive);
        assertEquals(olderThanAndEarnMoreList, hr_rep.olderThanAndEarnMore(allEmployees, hr));
    }

    @Test
    void practiceLengthLongerThan() {
        var days = 7;
        var salaryAfterRaise = intern.getSalary().multiply(BigDecimal.valueOf(1.05));
        var listInterns = new ArrayList<Intern>();
        listInterns.add(intern);
        hr_rep.practiceLengthLongerThan(listInterns,days);
        var newSalary = listInterns.get(0).getSalary();
        var listOutcome = listInterns.get(0);
        assertEquals(salaryAfterRaise, newSalary);
        assertEquals(intern, listOutcome);
    }

    @Test
    void seniorityLongerThan() {
        var months = 2;
        var outputList = new ArrayList<Employee>();
        outputList.add(manager);
        outputList.add(executive);
        var bonusPercentage = bonus_plan.annual_manager;
        var comparisonList = hr_rep.seniorityLongerThan(allEmployees, months);
        assertEquals(outputList, comparisonList);
    }

    @Test
    void seniorityBetweenOneAndThreeYears() {
        List<Employee> emptyList = new ArrayList<>();
        assertEquals(emptyList, hr_rep.seniorityBetweenOneAndThreeYears(allEmployees));
    }

    @Test
    void testSeniorityLongerThan() {

        var listOutcome = hr_rep.seniorityLongerThan(allEmployees, it_guy);
        var listCheck = new ArrayList<Employee>();
        listCheck.add(manager);
        listCheck.add(hr_rep);
        assertEquals(listCheck, listOutcome);

    }

    @Test
    void seniorityBetweenTwoAndFourYearsAndAgeGreaterThan() {
        List<Employee> emptyList = new ArrayList<>();
        assertEquals(emptyList, hr_rep.seniorityBetweenTwoAndFourYearsAndAgeGreaterThan(allEmployees, (short) 25));
    }
}