package psieminski.tests;

import static org.junit.jupiter.api.Assertions.*;
import psieminski.*;
import psieminski.enums.Department;
import psieminski.enums.bonus_plan;
import psieminski.enums.intern_type;

import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

class PeopleManageRightsTest {

    private Company company = new Company("Apple");
    private Manager manager = new Manager("Andrew", "A Manager", LocalDate.parse("1968-11-30") , bonus_plan.annual_manager, Department.IT, BigDecimal.valueOf(65000.00), BigDecimal.valueOf(0.49),
                                        company, 2, null, null);
    private Manager executive = new Manager("Theo", "The Boss", LocalDate.parse("1948-11-30") , bonus_plan.annual_executive, Department.HR, BigDecimal.valueOf(135000.00), BigDecimal.valueOf(0.64),
            company, 3, null, null);
    private HR hr_rep = new HR("John", "From HR", LocalDate.parse("1971-05-02") ,bonus_plan.annual_subordinate, Department.HR, BigDecimal.valueOf(35000.00), BigDecimal.valueOf(0.31),
            company, 1, executive);
    private IT it_guy = new IT("Carl", "From IT", LocalDate.parse("1985-05-02") ,bonus_plan.annual_subordinate, Department.IT, BigDecimal.valueOf(69000.00), BigDecimal.valueOf(0.34),
            company, 4, manager);
    private Intern intern = new Intern("Lark", "An Intern", LocalDate.parse("2000-05-02") ,bonus_plan.annual_subordinate, Department.IT, BigDecimal.valueOf(14000.00), BigDecimal.valueOf(0.19),
            company, 5, manager, intern_type.six_months);

    private void assignEmployeesToCompany(){
        company.employees.add(manager);
        company.employees.add(executive);
        company.employees.add(hr_rep);
        company.employees.add(it_guy);
        company.employees.add(intern);
    }

    private void assignHireDate() {
        manager.assignHireDate(hr_rep, "2021-01-01");
        manager.assignHireDate(it_guy, "2021-02-02");
        manager.assignHireDate(intern, "2021-03-03");
        manager.assignHireDate(manager, "2020-12-12");
        manager.assignHireDate(executive, "2020-11-11");
    }

    @org.junit.jupiter.api.Test
    void getLongestSeniority() throws ParseException {

        assignHireDate();
        assignEmployeesToCompany();

        assertEquals(executive,manager.findLongestSeniority());

    }

    @org.junit.jupiter.api.Test
    void calculateCostOfBonuses() {
        assignEmployeesToCompany();

        BigDecimal bonus_manager = BigDecimal.valueOf(0.15).multiply(manager.getSalary());
        BigDecimal bonus_exec = BigDecimal.valueOf(0.2).multiply(executive.getSalary());
        BigDecimal bonus_hr = BigDecimal.valueOf(0.1).multiply(hr_rep.getSalary());
        BigDecimal bonus_it = BigDecimal.valueOf(0.1).multiply(it_guy.getSalary());
        BigDecimal bonus_intern = BigDecimal.valueOf(0.1).multiply(intern.getSalary());
        BigDecimal sum_bonuses = bonus_manager.add(bonus_exec).add(bonus_hr).add(bonus_it).add(bonus_intern);

        assertEquals(sum_bonuses, executive.countCostAllBonus());
    }

    @org.junit.jupiter.api.Test
    void highestSalaryWithoutBonus() {
        assignEmployeesToCompany();
        assertEquals(BigDecimal.valueOf(135000.00), hr_rep.highestSalaryNoBonus());
    }

    @org.junit.jupiter.api.Test
    void highestSalaryWithBonus() {
        assignEmployeesToCompany();
        assertTrue(BigDecimal.valueOf(162000.00).compareTo(executive.highestSalaryWithBonus()) == 0);
    }

    @org.junit.jupiter.api.Test
    void nameBeginsWith() {
        assignEmployeesToCompany();
        hr_rep.assignOneToOneManagerSubordinate(manager, it_guy);
        hr_rep.assignOneToOneManagerSubordinate(manager,hr_rep);
        hr_rep.assignOneToOneManagerSubordinate(manager, intern);
        assertEquals(intern.getLast_name(), hr_rep.employeesSurnameBeginsWithX("A", manager).get(0).getLast_name());
    }

    @org.junit.jupiter.api.Test
    void earnMoreThanX() {
        assignEmployeesToCompany();
        assertEquals(company.getEmployees(), hr_rep.earnMoreThanN(BigDecimal.valueOf(1000.00)));
    }

    @org.junit.jupiter.api.Test
    void getAllSubordinates() {
        assignEmployeesToCompany();
        List<Employee> subordinate = new ArrayList<Employee>();
        subordinate.add(hr_rep);
        subordinate.add(it_guy);
        subordinate.add(intern);
        assertEquals(subordinate, manager.getAllSubordinates());
    }

}