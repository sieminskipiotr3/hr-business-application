package psieminski.tests;

import org.junit.jupiter.api.Test;
import psieminski.Company;
import psieminski.HR;
import psieminski.Manager;
import psieminski.enums.Department;
import psieminski.enums.bonus_plan;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    Company company = new Company("Apple");

    Manager manager = new Manager("Andrew", "Manager", LocalDate.parse("1968-11-30"), bonus_plan.annual_manager, Department.IT, BigDecimal.valueOf(65000.00), BigDecimal.valueOf(0.49),
            company, 2, null, null);
    HR hr_rep = new HR("John", "Testing", LocalDate.parse("1971-05-02") , bonus_plan.annual_subordinate, Department.HR, BigDecimal.valueOf(35000.00), BigDecimal.valueOf(0.31),
            company, 1, manager);

    public void setHireDates() {
        hr_rep.setHire_date("2015-10-05");
        hr_rep.setHireDateForEmployee(manager ,"2012-05-05");
    }

    @Test
    void isOlderThan() {
        assertTrue(true, String.valueOf(manager.isOlderThan(hr_rep)));
    }

    @Test
    void isYoungerThan() {
        assertFalse(false, String.valueOf(manager.isYoungerThan(hr_rep)));
    }

    @Test
    void compareAge() {
        assertEquals("Manager is older than Testing", manager.compareAge(hr_rep));
    }

    @Test
    void seniorityLongerThanYears() {
        setHireDates();
        var years = 4;
        assertTrue(true, String.valueOf(manager.seniorityLongerThanYears(years)));
    }

    @Test
    void seniorityLongerThanMonths() {
        setHireDates();
        var months = 10548;
        assertFalse(false, String.valueOf(hr_rep.seniorityLongerThanMonths(months)));
    }

    @Test
    void bonusGreaterThan() {
        var bonus = BigDecimal.valueOf(1500);
        assertTrue(true, String.valueOf(manager.bonusGreaterThan(bonus)));

    }
}