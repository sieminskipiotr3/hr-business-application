package psieminski;

import psieminski.enums.Department;
import psieminski.enums.bonus_plan;
import psieminski.enums.intern_type;

import java.math.BigDecimal;
import static java.time.temporal.ChronoUnit.DAYS;
import java.time.LocalDate;

public class Intern extends Subordinate {

    public intern_type internship_type;
    public long lengthOfPractice;

    public Intern(String first_name, String last_name, LocalDate birth_date,
                  bonus_plan bonus, Department department, BigDecimal salary, BigDecimal tax_rate, Company company,
                  Integer id, Manager manager, intern_type internship_type) {
        super(first_name, last_name, birth_date ,bonus, department, salary, tax_rate, company, id, manager);
        this.internship_type = internship_type;
    }

    public intern_type getInternship_type() {
        return internship_type;
    }

    public void setInternship_type(intern_type internship_type) {
        this.internship_type = internship_type;
    }

    public int getInternshipLengthDays() {
        LocalDate today = LocalDate.now();
        if(getHire_date() == null) {throw new IllegalStateException("Please ask HR/Manager to assign hireDate");}
        return (short) DAYS.between(getHire_date(), today);
    }

    public void setLengthOfPractice() {
        if(this.getHire_date() == null) {throw new IllegalStateException("Please ask HR to set hireDate");}
        LocalDate today = LocalDate.now();
        this.lengthOfPractice = DAYS.between(getHire_date(), today);
    }

}
