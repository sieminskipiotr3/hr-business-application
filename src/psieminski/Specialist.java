package psieminski;


import psieminski.enums.Department;
import psieminski.enums.bonus_plan;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Specialist extends Subordinate {

    public Specialist(String first_name, String last_name, LocalDate birth_date,
                      bonus_plan bonus, Department department, BigDecimal salary, BigDecimal tax_rate, Company company,
                      Integer id, Manager manager) {
        super(first_name, last_name, birth_date ,bonus, department, salary, tax_rate, company, id, manager);
    }
}
