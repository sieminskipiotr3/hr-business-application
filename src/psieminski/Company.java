package psieminski;

import java.util.ArrayList;
import java.util.List;

public class Company {

    public String name;
    public List<Employee> employees = new ArrayList<Employee>();

    public Company(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Employee> getEmployees() {
        if (employees == null) {setEmployees(new ArrayList<Employee>());}
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }
}
